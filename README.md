# Vagrant Debian - General

## What's the point?
This is just a template repository used by the [Vagrant Debian Customizer](https://gitlab.com/rubyonracetracks/vagrant-debian-custom).

## Why do you rely on geerlingguy as the source of the Vagrant base box?
* I had difficulty with file syncing for some of Vagrant base boxes I tried, including the "generic" and Debian's official non-contrib box.
* While the file syncing worked for Debian's official contrib box, the geerlingguy box is published more often.  This means that the geerlingguy box is likely more up-to-date than Debian's official box.
* All of the remaining Vagrant base boxes are less popular than those from geerlingguy.

## What's added during provisioning?
* Xfce desktop environment
* Web browser
* Node Version Manager (NVM)
* Ruby Version Manager (RVM)
* PostgreSQL
* SQLite
* Docker

## Why do you include the Xfce desktop environment, LightDM and a web browser in the Vagrant box?  And why is the Vagrantfile set to display the VirtualBox GUI when booting the machine?
* Including the GUI tools and displaying the GUI allows you to see how your development environment would look in a typical host environment.  When all of the other members of the project rely on their host environments for their development environments, you'll have an easier time if you start off on the same wavelength.  Once you get the project working with the aid of Vagrant, you'll be in a much better position to Dockerize it.
* Some projects have tests that involve a web browser.  These tests may not work in a development environment lacking a web browser.

## Why do you include Docker in the Vagrant box?
The setup instructions for some projects rely on the host system for some parts of the project and a Docker container for another part.  While you can run a Docker container in a Vagrant box, you cannot run a Docker container within a Docker container.
