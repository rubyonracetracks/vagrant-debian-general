#!/bin/bash
set -e

vagrant halt -f
vagrant destroy -f
vagrant box update
bash login.sh
